## HUBSPOT CRM AUTOMATION

`AUTHOR` - _Rajeshwar Sharma_

`TOOLs & TECHNOLOGIES ` - _WebdriverIO , Javascript, Cucumber, Gherkin, Allure Reporting, Netlify, Chai, VSCode, NodeJS_


#### SETTING UP THIS PROJECT 

### Quick start :rocket:

Choose one of the following options:

1. Download or Clone the git repo — 
```bash 
git clone https://javapytuts@bitbucket.org/javapytuts/wdio-cucumber.git
```

2. Then:
- Copy the files to your project into a directory like `/integrationtests` (note the hidden files!)

3. Clean the project (Optional): 
- *On OSX/Linux:*
-- Run `npm run clean`

- *On Windows:*
-- Remove the directories `/.git`, `/.github`
-- Remove the files `wdio.BUILD.conf.js`
-- Remove all the demo features from the `/src/features` directory

1. Install the dependencies (`npm install`)

Now you are ready to write your own features.


#### Writing our first feature file :green_book:

Tests are written in [Gherkin syntax](https://cucumber.io/docs/reference)
that means that you write down what's supposed to happen in a real language. All test files are located in
`./src/features/*` and have the file ending `.feature`. You will already find some test files in that
directory. They should demonstrate, how tests could look like. Just create a new file and write your first
test.

__myFirstTest.feature__ :green_book:
```gherkin
Feature:
    In order to keep my product stable
    As a developer or product manager
    I want to make sure that everything works as expected

Scenario: Check title of website after search
    Given I open the url "http://google.com"
    When I set "WebdriverIO" to the inputfield "#lst-ib"
    And I press "Enter"
    Then I expect that the title is "WebdriverIO - Google Search"

Scenario: Another test
    Given ...

```


Download .msi file and install the same. 
> _DISCLAIMER: Make sure you should have node.js installed on your system_

Now create your own feature file withn ./src/features/ folder with any name. In my case, it is myFirstFeature.js

Now, let make the changes to the existing feature file to execute our only feature file created just now
```js
Before-----------
specs: [
        './src/features/**/*.feature',
    ],
    // Patterns to exclude.

After-------------
 specs: [
        './src/features/myFirstFeature.feature',
    ],
    // Patterns to exclude.
```
As per the README file for this project we need to execute the command 

```bash
npm run execute
```

But, when we run the command we will get below issue - 
```bash
[0-3] 2020-02-28T11:06:32.006Z ERROR webdriver: Request failed due to Error: connect ECONNREFUSED 127.0.0.1:4444
    at TCPConnectWrap.afterConnect [as oncomplete] (net.js:1137:16)
[0-3] Error: Failed to create session.
Unable to connect to "127.0.0.1:4444", make sure browser driver is running on that address.
If you use services like chromedriver see initialiseServices logs above or in wdio.log file.
[0-3] 2020-02-28T11:06:32.006Z ERROR webdriver: Error: connect ECONNREFUSED 127.0.0.1:4444
    at TCPConnectWrap.afterConnect [as oncomplete] (net.js:1137:16)
2020-02-28T11:06:32.007Z ERROR @wdio/runner: Error: Failed to create session.
Unable to connect to "127.0.0.1:4444", make sure browser driver is running on that address.
If you use services like chromedriver see initialiseServices logs above or in wdio.log file.
    at startWebDriverSession (C:\Users\rajeshwar.sharma\Desktop\WDIO-SETUP\integrationtest\node_modules\webdriver\build\utils.js:45:11)
    at processTicksAndRejections (internal/process/task_queues.js:97:5)
2020-02-28T11:06:32.121Z DEBUG @wdio/local-runner: Runner 0-3 finished with exit code 1
[0-3] FAILED in chrome - C:\Users\rajeshwar.sharma\Desktop\WDIO-SETUP\integrationtest\src\features\checkTitle.feature
```

**Let's see how we can get rid of this_**
Firstly, we need to run any local server but in our case we can run selenium-standallone server

First, install the selenium-standalone using - 
```bash
npm install --save-dev selenium-standalone
```

Get this message on successful installation
success Saved lockfile. 
success Saved 1 new dependency.

Run this -  
```bash
./node_modules/.bin/selenium-standalone install
```

Start the selenium-standalone server 
```bash
$ ./node_modules/.bin/selenium-standalone start 
```

Now, if we try to run test case using - 
```bash
npm run execute
```

This time we will have another error 
```bash
[0-0] Error in "Test the attributes of a given element: The CSS attribute "color" of a element should not be "blue": The
n I expect that the css attribute "color" from element "#cssAttributeComparison" is not " rgba(0,255,0,1)""
Can't call getCSSProperty on element with selector "#cssAttributeComparison" because element wasn't found

[0-0] 2020-02-28T11:19:16.085Z INFO webdriver: Retrying 1/3
2020-02-28T11:19:16.085Z INFO webdriver: [POST] http://127.0.0.1:4444/wd/hub/session/497041e7103b1e4305056046efa36ad2/ur
l
2020-02-28T11:19:16.085Z INFO webdriver: DATA { url: 'http://localhost:8080/' }
```
DONT WORRY :)

Lets try to fix it - 

_Go to wdio.config file and change baseUrl to  baseUrl: 'http://localhost:4444',
from   baseUrl: 'http://localhost:8080'_

This time the browser will open but it will open selenium page but not google page


So lets open wdio.config file again and made the changes - add below line 
```bash
services: ['selenium-standalone'],

var baseUrl; // add this at the top of wdio.config file and change baseUrl: 'http://localhost:4444', to below  
// parameter starts with "/", then the base getUrl gets prepended.
    baseUrl: baseUrl,
    //
```

When we try to install webdriverio, we get 

```shell
info fibers_node_v8@3.1.5: The engine "node" is incompatible with this module. Expected version ">=8.0.0 <10.0.0". Got "12.16.0"        info "fibers_node_v8@3.1.5" is an optional dependency and failed compatibility check. Excluding it from installation.                   [3/4] Linking dependencies.                                                                                                      error An unexpected error occurred: "EPERM: operation not permitted, unlink 'C:\\Users\\rajeshwar.sharma\\Desktop\\WDIO-SETUP\\integrati
ontest\\node_modules\\selenium-standalone\\.selenium\\chromedriver\\2.43-x64-chromedriver'"
```

_In my case the raeson was the nodejs version.   
The engine "node" is incompatible with this module. Expected version ">=8.0.0 <10.0.0". Got "12.16.0"_

```shell
error An unexpected error occurred: "EPERM: operation not permitted, unlink 'C:\\Users\\rajeshwar.sharma\\Desktop\\WDIO-SETUP\
\integrationtest\\node_modules\\selenium-standalone\\.selenium\\chromedriver\\2.43-x64-chromedriver'" 
```


##### Setting Up Allure Report :bar_chart:
To generate allure reports we need to install -
```bash
npm i @wdio/allure-reporter
```

To run and open report, we need to install -
```bash
npm install -g allure-commandline --save-dev
```

Once we install there plugins, we need to make few changes to our wdio.config file as 

```js
reporters: ["allure"],
    
  reportOptions: {
      outputDir: "allure-results",
      disableWebdriverStepsReporting: true,
      disableWebdriverScreenshotsReporting: true
    },
```

Now we are good to execute our test cases and generate allure reports

```shell
npm run test 
allure generate ./allure-results
allure open
```

Since, the reports are generated locally and not possible to share across, I am using [NETLIFY](https://www.netlify.com/) for hosting my report online so it can be shared across.

_Here, is the link of Allure Test Report_

https://app.netlify.com/sites/xenodochial-goldwasser-7b3491/deploys


###### Different options to run through CMD :point_down:
```bash
   57  yarn run wdio --base-url=https://www.google.com
   59  yarn run wdio --spec=./src/features/githubSearch.feature
   60  yarn run wdio --spec=./src/features/inputfieldDone.feature
   61  yarn run wdio --spec=./src/features/inputfieldDone.feature --baseUrl=https://www.google.com
   62  yarn run wdio --spec=./src/features/githubLogin.feature
   63  yarn run wdio --spec=./src/features/urlValidation.feature --baseUrl=http://127.0.0.1:8080
   64  yarn run wdio --spec=./src/features/withinViewport.feature --baseUrl=https://www.google.com
   65  yarn run wdio --spec=./src/features/withinViewport.feature --baseUrl=https://www.digitalocean.com/
   66  yarn run wdio --spec=./src/features/isExisting.feature
   68  yarn run wdio --spec=./src/features/isExisting.feature --baseUrl=https://www.google.com
   69  yarn run wdio --spec=./src/features/isEmpty.feature --baseUrl=https://www.google.com
   70  yarn run wdio --spec=./src/features/elementVisibility.feature
   71  yarn run wdio --spec=./src/features/elementVisibility.feature --cucumberOpts.tagExpression=@Pending
   72  yarn run wdio --spec=./src/features/cookie.feature --cucumberOpts.tagExpression=@Pending
   73  yarn run wdio --spec=./src/features/cookie.feature --cucumberOpts.tagExpression='@Pending'
   75  yarn run wdio --spec=./src/features/githubSearchDone.feature --cucumberOpts.tagExpression='@Regression'
   77  yarn run wdio --spec=./src/features/githubSearchDone.feature --cucumberOpts.tagExpression='@Regression or @Sanity'
   79  yarn run wdio --spec=./src/features/githubSearchDone.feature --cucumberOpts.tagExpression='@Regression and @Sanity'
```

###### Running using npm :point_down:
```shell
114  ./node_modules/.bin/wdio wdio.conf.js
115  npm run test
116  ./node_modules/.bin/wdio --spec=./features/login.feature
117  ./node_modules/.bin/wdio --spec=./features/login.feature --cucumberOpts.tagExpression=@Regression
118  ./node_modules/.bin/wdio --spec=./features/login.feature --cucumberOpts.tagExpression=@Sanity
122  npm run test --spec=./features/login.feature --cucumberOpts.tagExpression=@Sanity
123* npm run test -- --spec=./features/login.feature --c
124  npm run test -- --spec=./features/login.feature --cucumberOpts.tagExpression=@Sanity
```

###### Here is the list of issues that I faced during setup and resolution for the same 

[ISSUEs & RESOLUTIONs ](https://bitbucket.org/javapytuts/wdio-cucumber/src/master/issues.md) :warning: