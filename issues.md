### ISSUES  & MITIGATIONS

###### ISSUE: :x:
```node
function timed out, ensure the promise resolves within 20000 milliseconds
```

###### RESOLUTION: :white_check_mark:
_increase the timeout key value in wdio.config.js_

timeout = 90000

###### ISSUE :x:
```node
[chrome  mac os x #0-0] expect is not defined
```
###### RESOLUTION: :white_check_mark:
Add chai assert/except/should in stepdefinition files or wdio.config.js file under before() hook as -
```js
before: function (capabilities, specs) {
        const chai = require('chai');
        const expect = chai.expect;
        const should = chai.should();
        const assert = chai.assert;

    }
```

###### ISSUE: :x:
Even after setting chai assertion - we are geeting the above error

```bash
[chrome  mac os x #0-0] 1) Check title of website after search When I set "WebdriverIO" to the inputfield "#lst-ib"
[chrome  mac os x #0-0] expect is not defined
```
###### RESOLUTION: :white_check_mark:
Add chai assert/except/should in stepdefinition files or wdio.config.js file under before() hook as -
```js
before: function (capabilities, specs) {
        const chai = require('chai');
        global.expect = chai.expect;
        global.should = chai.should();
        global.assert = chai.assert;

    }
```

###### ISSUE: :x:
```node
2020-03-17T14:08:02.389Z ERROR @wdio/cli:utils: A service failed in the 'onPrepare' hook
```
###### RESOLUTION: :white_check_mark:
We will be having 
`services: ['chromedriver','selenium-standalone'],`
 in wdio.config.js 
Also, the selenium-standalone server is running too through terminal/cmd

###### ISSUE: :x:
So, the interesting stuff. Once you do the above resolution, you encounter another issue 
```node
[0-0] 2020-03-17T14:12:34.646Z ERROR @wdio/runner: Error: Failed to create session.
It seems you are running a Selenium Standalone server and point to a wrong path. Please set `path: '/wd/hub'` in your wdio.conf.js!
```
###### RESOLUTION: :white_check_mark:
And the reason for this is - the value of 'path' key in wdio.config.js file
```js
// Override default path ('/wd/hub') for chromedriver service.
path: '/',
//
path: '/wd/hub'
```
###### ISSUE: :x:
_`element is not interactable`_

###### RESOLUTION: :white_check_mark:
Change element locator
 
###### ISSUE: :x:
```node
unknown error: Element <span class="private-loading-button__content private-button--internal-spacing">...</span> is not clickable at point (715, 499). Other element would receive the click: <span aria-hidden="true" class="UIIcon__IconContent-sc-19tpupa-0 dsgqxr"></span>
```

###### RESOLUTION: :white_check_mark:
Try injecting JS code
browser.execute(Script, arguments)

###### ISSUE: :x:
```node
`[chrome 80.0.3987.132 Mac OS X #0-0] Step "And I enter "email" as mike.tyson@hollywood.com" is not defined. You can ignore this error by setting cucumberOpts.ignoreUndefinedDefinitions as true
```
We were getting above issue when we were using Scenario Outline as 
```gherkin
        And I enter "email" as <email_id>
        And I enter "firstname" as <first_name>
```

###### RESOLUTION: :white_check_mark:
Here, in JS we need to use double quotes "" as 
```gherkin
        And I enter "email" as "<email_id>"
        And I enter "firstname" as "<first_name>"
``` 

###### ISSUE : :x:
```node
[0-0] 2020-03-18T09:12:22.568Z ERROR @wdio/local-runner: Failed launching test session: Error: Invalid reporters config
```

###### RESOLUTION: :white_check_mark:
Remove "spec" from -
```js
  reporters: [
    "spec",
    "allure",
    {
      outputDir: "allure-results",
      disableWebdriverStepsReporting: true,
      disableWebdriverScreenshotsReporting: true
    }
  ],
```

And change above set up in wdio.config.js file as below -

```js
reporters: ["allure"],

reportOptions: {
      outputDir: "allure-results",
      disableWebdriverStepsReporting: true,
      disableWebdriverScreenshotsReporting: true
}
```

