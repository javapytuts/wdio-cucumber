Feature: In order to track customer details
    As an admin user
    I should able to add multiple contacts to CRM

    Background:
        Given I have accessed login url "https://app.hubspot.com/login"
        When I enter username "sharma.rajeshwar6@gmail.com"
        And I enter password "Rajeshwar@96"
        And I enter login button
        Then I should able to see "Restart demo"

    Scenario Outline: To validate admin user able to add multiple contacts
        When I click on "Contacts" menu item
        And I click on "Contacts" from drop down menu list
        And I click on "Create contact" button
        And I enter "email" as "<email_id>"
        And I enter "firstname" as "<first_name>"
        And I enter "lastname" as "<last_name>"
        And I enter "jobtitle" as "<job_title>"
        And I enter "phone" as "<phone_no>"
        And I select "<lifecycle_stage>" value from "Lifecycle stage" dropdown
        And I select "<lead_status>" value from "Lead status" dropdown
        And I click on "Create contact" button to add
        Examples:
            | email_id                 | first_name | last_name | job_title | phone_no  | lifecycle_stage | lead_status |
            | mike.tyson@hollywood.com | Mike       | Tyson     | Actor     | 253363728 | Opportunity     | Connected   |
            | peter.parker@marvel.com  | Peter      | Parker    | Spier Man | 244353365 | Opportunity     | Connected   |