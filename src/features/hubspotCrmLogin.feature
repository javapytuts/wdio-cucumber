Feature: As an admin, I should able to login to hubspot CRM

    @Sanity @Regression
    Scenario: To validate the login functionality using valid credentials
        Given I have accessed login url "https://app.hubspot.com/login"
        When I enter username "sharma.rajeshwar6@gmail.com"
        And I enter password "Rajeshwar@96"
        And I enter login button
        Then I should able to see "Restart demo"

    @Sanity @Regression
    Scenario: To validate the login functionality using invalid credentials
        Given I have accessed login url "https://app.hubspot.com/login"
        When I enter username "sharma.rajeshwar9@gmail.com"
        And I enter password "password"
        And I enter login button
        Then I should able to see "Please check your entry and try again."
    @Sanity @Regression
    Scenario: To validate the login functionality using invalid credentials
        Given I have accessed login url "https://app.hubspot.com/login"
        When I enter username "sharma.rajeshwar6@gmail.com"
        And I enter password "password"
        And I enter login button
        Then I should able to see "entered an invalid password"