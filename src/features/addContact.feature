Feature: In order to track customer details
As an admin user
I should able to add contacts to CRM

Background: 
        Given I have accessed login url "https://app.hubspot.com/login"
        When I enter username "sharma.rajeshwar6@gmail.com"
        And I enter password "Rajeshwar@96"
        And I enter login button
        Then I should able to see "Restart demo"

Scenario: To validate admin user able to add contact
        When I click on "Contacts" menu item
        And I click on "Contacts" from drop down menu list
        And I click on "Create contact" button
        And I enter "email" as "tony.stark@avengers.com"
        And I enter "firstname" as "Tony"
        And I enter "lastname" as "Stark"
        And I enter "jobtitle" as "Iron Man"
        And I enter "phone" as "123242578"
        And I select "Customer" value from "Lifecycle stage" dropdown
        And I select "In progress" value from "Lead status" dropdown
        And I click on "Create contact" button to add

