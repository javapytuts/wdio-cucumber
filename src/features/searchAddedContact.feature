Feature: In order to get the details of added Contacts
    As an admin user
    I should able to search Contacts by firstname & lastname

    Background:
        Given I have accessed login url "https://app.hubspot.com/login"
        When I enter username "sharma.rajeshwar6@gmail.com"
        And I enter password "Rajeshwar@96"
        And I enter login button
        And I click on "Contacts" menu item
        And I click on "Contacts" from drop down menu list

    @Sanity @Regression
    Scenario: To validate the search functionality of Contacts using firstname
        When I enter search text "Mike"
        Then  I should able to see "Mike Tyson" under column 2
        And  I should able to see "mike.tyson@hollywood.com" under column 3
        And  I should able to see "253363728" under column 4