Feature:
    In order to keep my product stable
    As a developer or product manager
    I want to make sure that everything works as expected

    @Sanity
    Scenario: Check title of website after search
        Given I open the url "http://google.com"
        When I set "WebdriverIO" to the inputfield "[name='q']"
        And I press "Enter"
        Then I expect that the title is "WebdriverIO - Google Search"