import { Then } from "cucumber";

Then("I should able to see {string}", function (expectedText) {
  console.log("Logged in successfully ....");
  browser.pause(3000);
  var actualTextXpath = "//*[contains(text(),'" + expectedText + "')]";
  console.log(actualTextXpath);
  var actualTextLocator = $(actualTextXpath);
  expect(actualTextLocator.getText()).to.contain(expectedText);
});


Then('I should able to see {string} under column {int}', function (expectedValue, columnNo) {
  var columnXpath;
  if (columnNo == 2) {
    columnXpath = "//tbody[@class='Tbody-lhne0s-0 hqlPlm']//tr/td[2]//a/span/span";
    var columnLocator = $(columnXpath);
    var name = columnLocator.getText();
    expect(name).to.equal(expectedValue);
  } else if (columnNo == 3) {
    columnXpath = "//tbody[@class='Tbody-lhne0s-0 hqlPlm']//tr/td[3]//a";
    var columnLocator = $(columnXpath);
    var email = columnLocator.getText();
    expect(email).to.equal(expectedValue);
  } else if (columnNo == 4) {
    columnXpath = "//tbody[@class='Tbody-lhne0s-0 hqlPlm']//tr/td[4]//span";
    var columnLocator = $(columnXpath);
    var phone = columnLocator.getText();
    expect(phone).to.equal(expectedValue);
  }

});



