import { When } from "cucumber";

When("I enter username {string}", function (email) {
    console.log("Entering user name ....");
    var usernameLocator = $("#username");
    usernameLocator.setValue(email);
});

When("I enter password {string}", function (upassword) {
    console.log("Entering password ....");
    var passwordLocator = $("#password");
    passwordLocator.setValue(upassword);
});

When("I enter login button", function () {
    console.log("Clicking on login button ....");
    var loginBtn = $("#loginBtn");
    loginBtn.click();
    browser.pause(4000);
});

When("I click on {string} menu item", function (menubarOption) {
    var menubarItem = "(//a[@role='menuitem'][contains(text(),'" + menubarOption + "')])[1]";
    console.log(menubarItem);
    var menuBarLocator = $(menubarItem);
    menuBarLocator.moveTo();
    browser.pause(3000)
    menuBarLocator.click();
    browser.pause(2000);
});

When("I click on {string} from drop down menu list", function (dropdownItem) {
    var dropdownOption = "(//a[@class='navSecondaryLink']//div[contains(text(),'" + dropdownItem + "')])[1]";
    console.log(dropdownOption);
    var dropdownLocator = $(dropdownOption);
    dropdownLocator.moveTo();
    dropdownLocator.click();
    browser.pause(2000);
});

When("I click on {string} button", function (string) {
    var buttonToClick = "//button[@type='button']//span[contains(text(),'" + string + "')]";
    var buttonToClickLocator = $(buttonToClick);
    buttonToClickLocator.click();
    browser.pause(2000);
});

When("I click on {string} button to add", function (string) {
    //var buttonToClick = "(//button[@type='button']//span[contains(text(),'" + string + "')])[2]";
    var buttonToClickLocator = $("button[data-selenium-test='base-dialog-confirm-btn']");
    buttonToClickLocator.moveTo();
    buttonToClickLocator.click();
    browser.pause(2000);
});

When("I enter {string} as {string}", function (field, value) {
    var inputText = "//input[@data-field='" + field + "']";
    var inputTextLocator = $(inputText);
    inputTextLocator.setValue(value);
    browser.pause(2000);
});

When("I select {string} value from {string} dropdown", function (value, dropdown) {
    var dropdown;
    if (dropdown.toUpperCase() == "LIFECYCLE STAGE") {
        dropdown = "//div[@data-selenium-test='property-input-lifecyclestage']";
    } else if (dropdown.toUpperCase() == "LEAD STATUS") {
        dropdown = "//div[@data-selenium-test='property-input-hs_lead_status']";
    }
    var downdownLocator = $(dropdown);
    downdownLocator.click();
    browser.pause(2000);
    var dropdownValue = "//span[@class='private-dropdown__item__label'][text()='" + value + "']";
    var dropdownValueLocator = $(dropdownValue);
    browser.pause(2000)
    dropdownValueLocator.click();
    browser.pause(2000);
});

When("I enter search text {string}", function (searchText) {
    browser.pause(2000);
    var searchBox = $("[data-selenium-test = 'list-search-input']");
    searchBox.setValue(searchText);
    browser.pause(2000);
});